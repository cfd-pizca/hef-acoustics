Following the FEniCS documentation, the Finite Element Method here is
based on

> Larson, Mats G., and Fredrik Bengzon. The finite element method:
> theory, implementation, and applications. Vol. 10. Springer Science
> & Business Media, 2013.

# Weak forms for the Helmholtz equation

Cartesian, and axisymmetric, see [this
file](doc/details/weak_forms.pdf)

# Perfectly matched layer PML

Not yet implemented, see issue #17
