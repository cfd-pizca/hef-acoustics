#+TITLE: Velocity field $\vec{u}$
#+AUTHOR:
#+OPTIONS: toc:nil ':t num:t
#+LaTeX_HEADER: \usepackage[T1]{fontenc}
#+LaTeX_HEADER: \usepackage[utf8]{inputenc}
#+LaTeX_HEADER: \usepackage{lmodern}
#+LaTeX_HEADER: \usepackage{physics}
#+LaTeX_HEADER: \addtolength{\parskip}{\baselineskip}
#+LaTeX_HEADER: \usepackage{enumitem}
#+LaTeX_HEADER: \setlist{noitemsep}
#+LaTeX_HEADER: \setlist[itemize]{parsep=0mm}
#+LaTeX_HEADER: \setlist[itemize]{partopsep=0mm}
#+LaTeX_HEADER: \setlist[itemize]{itemsep=0mm}
#+LaTeX_HEADER: \setlist[itemize]{topsep=0mm}

#+LaTeX: \pagebreak

* Main equation

  According to standard acoustics hypotheses, and conventions chosen
  in this repository, the relation between acoustic pressure field and
  velocity field considered here is:
  \begin{align*}
  \vec{u} = \frac{-1}{i\omega\rho_0} \nabla p
  \end{align*}
  You can find the details for the obtention of this equation below.

* Real and imaginary parts

  In order to make the implementation in this repository, real an
  imaginary parts of the fields were explicitly considered.
  \begin{align*}
   \vec{u} &= \frac{-1}{i \omega \rho_0} \nabla p \\
   \vec{u}_r + i \vec{u}_i &= \frac{i}{\omega \rho_0} \nabla (p_r + i p_i) \\
               &= - \frac{1}{\omega \rho_0} \nabla p_i
                  + i \frac{1}{\omega \rho_0} \nabla p_r
  \end{align*}
  Then
  \begin{align}
  \label{eq:I1}
  \vec{u}_r &= - \frac{1}{\omega \rho_0} \nabla p_i \\
  \label{eq:I2}
  \vec{u}_i &= \frac{1}{\omega \rho_0} \nabla p_r
  \end{align}

* Axisymmetric case

  TODO: Check consistency for variable names with respect to other
  documents.

  When considering cylindrical coordinates, the gradient expression
  takes the form
  \begin{align*}
  \nabla p
  =
  \hat{r} \pdv{p}{r}
  + \hat{\phi} \frac{1}{r} \pdv{p}{\phi}
  + \hat{z}    \pdv{p}{z}
  \end{align*}

  In the axisymmetric case nothing depends on $\phi$, then
  $\pdv{p}{\phi} = 0$
  and
  \begin{align*}
  \nabla p
  =
  \hat{r} \pdv{p}{r}
  + \hat{z}    \pdv{p}{z}
  \end{align*}

  That is to say, the operation to get the gradient in the
  axisymmetric case is identical to the 2D cartesian case. Then there
  is no need to have special attention when calculating the velocity
  field in the axisymmetric case.

* Implementation

  You can find the code corresponding to \eqref{eq:I1} and
  \eqref{eq:I2} in the file ~hef/velocity.py~

  You can also see 2 demos exhibiting the different components of the
  velocity field:
  - ~demo/velocity_check_2D~
  - TODO: 3D velocity demo

  There you can check that the correspondence of the Python indices to
  the components of the velocity, as a tensor, and the way you can
  access these components in Paraview, is the following.

** Two spatial dimensions, 2D

   | Python | Paraview component | math         |
   |--------+--------------------+--------------|
   | u[0]   |                  0 | $\Re{u_{x}}$ |
   | u[1]   |                  1 | $\Re{u_{y}}$ |
   | -      |                  2 | -            |
   | u[2]   |                  3 | $\Im{u_{x}}$ |
   | u[3]   |                  4 | $\Im{u_{y}}$ |
   | -      |                  5 | -            |
   | -      |                  6 | -            |
   | -      |                  7 | -            |
   | -      |                  8 | -            |

   For the ~Python~ column, the object ~u~ is what you get calling the
   method ~split()~ of the velocity field object ~u_c~. See for example
   ~demo/velocity_check_2D/demo_A.py~, and ~hef/mic.py~.

   Paraview's "Magnitude" is corresponding to (TODO: Check this
   expression):

   \begin{align*}
   \sqrt{ \sum_n (\text{component n})^2 }
   \end{align*}

** Three spatial dimensions, 3D

   TODO: This is not right. Create a demo to check every parameter in
   this table.

   | Python | Paraview component | math         |
   |--------+--------------------+--------------|
   | u[0]   |                  0 | $\Re{u_{x}}$ |
   | u[1]   |                  1 | $\Re{u_{y}}$ |
   | u[2]   |                  2 | $\Re{u_{z}}$ |
   | u[3]   |                  3 | $\Im{u_{x}}$ |
   | u[4]   |                  4 | $\Im{u_{y}}$ |
   | u[5]   |                  5 | $\Im{u_{z}}$ |
   | u[6]   |                  6 | -            |
   | u[7]   |                  7 | -            |
   | u[8]   |                  8 | -            |

   For the ~Python~ column, the object ~u~ is what you get calling the
   method ~split()~ of the velocity field object ~u_c~. See for example
   ~demo/velocity_check_2D/demo_A.py~, and ~hef/mic.py~.

   Paraview's "Magnitude" is corresponding to (TODO: Check this
   expression):

   \begin{align*}
   \sqrt{ \sum_n (\text{component n})^2 }
   \end{align*}

* Development for the main relation

  Let $\tilde{p}$ be the acoustic pressure field, and
  $\tilde{\vec{u}}$ be the velocity field[fn:1]. Then, if we take the
  momentum conservation equation \cite{blackstock2001}, equations D-2
  (chap. 1)
  \begin{align*}
  \rho\left(
    \pdv{\tilde{\vec{u}}}{t}
    +
    (\tilde{\vec{u}} \cdot \nabla) \tilde{\vec{{u}}}
  \right) + \nabla{\tilde{p}} = 0
  \end{align*}
  when taking first order terms only, we have:
  \begin{align}
  \label{eq:L2}
  \rho_0\pdv{\tilde{\vec{u}}}{t} + \nabla{\tilde{p}} = 0
  \end{align}
  where $\rho_0$ is the mass density at equilibrium.

  If we now use the time-harmonic hypothesis
  \begin{align}
  \label{eq:TH:p}
  \tilde{p}(\vec{x},t) = p(\vec{x}) e^{i\omega t}
  \end{align}
  and the corresponding hypothesis for the velocity
  \begin{align}
  \label{eq:TH:u}
  \tilde{\vec{u}}(\vec{x},t) = \vec{u}(\vec{x}) e^{i\omega t}
  \end{align}
  equation \eqref{eq:L2} take the form
  \begin{align}
  \rho_0 i \omega \vec{u} + \nabla{p} = 0
  \end{align}

  Then
  \begin{align}
  \vec{u} = \frac{-1}{i\omega\rho_0} \nabla{p}
  \end{align}

* Magnitude of $\vec{u}$

  Since the velocity $\vec{u}$ is a vector whose components are
  complex numbers,
  \begin{align*}
  \vec{u} = (u_x, u_y, u_z) \qquad u_x, u_y, u_z \in \mathbb{C}
  \end{align*}
  it is not straightforward to say how the magnitude
  of the velocity should be calculated.

  TODO: find a reference for this discussion.

  Thinking in terms of equations, some of the potentially confusing
  possibilities are:
  1.
     \begin{align*}
     \sqrt{u_x^2 + u_y^2 + u_z^2}
     \end{align*}
     which is a complex number.
  2.
     \begin{align*}
     \left| \sqrt{u_x^2 + u_y^2 + u_z^2} \right|
     \end{align*}
     which is a real number
  3.
     \begin{align*}
     \sqrt{|u_x|^2 + |u_y|^2 + |u_z|^2}
     \end{align*}
     which is a real number.
  4.
     \begin{align*}
     \sqrt{u_{x,r}^2 + u_{x,i}^2 + u_{y,r}^2 + u_{y,i}^2 + u_{z,r}^2 + u_{z,i}^2}
     \end{align*}
     which is a real number, and makes no sense, but is (I think) what
     paraview shows as "magnitude".

 Thinking geometrically, the complex nature of $\vec{u}$ is simply a
 way to express the fact that this vector is in "harmonic
 motion". More specifically, each of its components is oscillating
 harmonically. And, the problem comes when the relative phase of this
 oscillation is considered.

 If all the components are in phase, the vector is always over the
 same line growing and shrinking in both directions. On the other
 hand, if these relative phases are not null, the vector rotates with
 its tip describing an ellipse. Moreover, in the case this ellipse is
 a circle, the vector does not changes its size.

** Plane waves

   Fortunately, in many of the cases where one needs the magnitude of
   $\vec{u}$ the propagation is a plane wave, then all the components
   of the velocity oscillate in phase, which means that in some
   coordinate system where the x-axis is the propagation axis, we have
   $u_y=u_z=0$.

   In this case, there is no ambiguity
   \begin{align*}
   |\vec{u}| = |u_x|
   \end{align*}

   This same case, in an arbitrary coordinate system, satisfies:
   \begin{align*}
   |\vec{u}| &= \sqrt{|u_x|^2 + |u_y|^2 + |u_z|^2}  \\
             &=  \left| \sqrt{u_x^2 + u_y^2 + u_z^2} \right|
   \end{align*}

* TODO

  - Complete these notes.

  - Use a citation reference for an explanation, or a specific
    reference, on why derivatives of the field variables $p$ and
    $\vec{u}$ have the same order than the field themselves.

# * References
#   :PROPERTIES:
#   :UNNUMBERED: t
#   :END:

#+BIBLIOGRAPHY: ../ref plain option:-d

* Footnotes

[fn:1] The use of tildes here is to leave the "untilded" variables for
complex amplitudes. See equations \eqref{eq:TH:p} and \eqref{eq:TH:u}.



* <<x>>                       :noexport:

# Local Variables:
# ispell-dictionary: "en"
# End:

#  LocalWords:  compatibilities PML backend eq untilded linearized
