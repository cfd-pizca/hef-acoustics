#+TITLE: Helmholtz equation
#+AUTHOR: HEF-Acoustics
#+OPTIONS: toc:nil ':t num:t
#+LaTeX_HEADER: \usepackage[T1]{fontenc}
#+LaTeX_HEADER: \usepackage[utf8]{inputenc}
#+LaTeX_HEADER: \usepackage{lmodern}
#+LaTeX_HEADER: \usepackage{physics}
#+LaTeX_HEADER: \addtolength{\parskip}{\baselineskip}
#+LaTeX_HEADER: \usepackage{enumitem}
#+LaTeX_HEADER: \setlist{noitemsep}
#+LaTeX_HEADER: \setlist[itemize]{parsep=0mm}
#+LaTeX_HEADER: \setlist[itemize]{partopsep=0mm}
#+LaTeX_HEADER: \setlist[itemize]{itemsep=0mm}
#+LaTeX_HEADER: \setlist[itemize]{topsep=0mm}

#+LaTeX: \pagebreak

* Helmholtz equation

  The version of the Helmholtz equation considered for this repository
  is
  \begin{align}
  \nabla^2 p + k^2 p = 0
  \end{align}
  where
  \begin{align*}
  k = \frac{\omega}{c} - i \alpha
  \end{align*}

* Derivation

  Consider the wave equation, in a version including viscous
  attenuation, [[cite:pierce2019acoustics][Pierce]] Eq. (10.3.13)
  \begin{align*}
  \nabla^2 \tilde{p}
  - \frac{1}{c^2}\pdv[2]{\tilde{p}}{t}
  + \frac{2}{c^4}\delta_{cl}\pdv[3]{\tilde{p}}{t}
  = 0
  \end{align*}
  where, $\delta_{cl}$ is a constant in terms of the properties of the
  fluid, including the dynamic viscosity $\mu$.
  
  Now, we assume an harmonic time dependency
  \begin{align*}
  \tilde{p}(\vec{x},t) = p(\vec{x}) e^{i\omega t}
  \end{align*}
  see that [[cite:pierce2019acoustics][Pierce]], Eq. (1.8.1), uses the other convention, having a
  minus sign within the exponential function.
  
  Then,
  \begin{align*}
  \nabla^2 p
  - \frac{1}{c^2}(i\omega)^2 p
  + \frac{2}{c^4}\delta_{cl} (i\omega)^3 p
  &= 0 \\
  \nabla^2 p
  + \left(   \frac{\omega^2}{c^2}
           - i \frac{2 \delta_{cl} \omega^3 }{c^4}
    \right) p
  &= 0
  \end{align*}
  
  Since the attenuation coefficient $\alpha$ is related to
  $\delta_{cl}$, by [[cite:pierce2019acoustics][Pierce]] Eq. (10.2.1)
  \begin{align*}
  \alpha = \frac{\omega^2 \delta_{cl}}{c^3}
  \end{align*}
  
  Then, we write
  \begin{align}
  \label{eq:k:noapprox}
  \nabla^2 p
  + \left(   \frac{\omega^2}{c^2}
           - i \frac{2 \omega }{c} \alpha
    \right) p
  &= 0
  \end{align}
  
  Which is the Helmholtz equation,
  \begin{align*}
  \nabla^2 p
  + k^2 p
  &= 0
  \end{align*}
  for a constant
  \begin{align}
  k^2 = \frac{\omega^2}{c^2} - 2 i \frac{\omega}{c}\alpha
  \end{align}
  
  However, Equation \eqref{eq:k:noapprox} is *not* what is implemented for
  the weak form in this repository. If we take
  \begin{align}
  \label{eq:approx}
  \frac{\omega}{c} \gg \alpha
  \end{align}
  Then, to first order
  \begin{align*}
  \left(\frac{\omega}{c} - i \alpha\right)^2
  =
  \frac{\omega^2}{c^2} + \alpha^2 - 2 i \frac{\omega}{c}\alpha
  =
  \frac{\omega^2}{c^2} - 2 i \frac{\omega}{c}\alpha
  \end{align*}
  
  Then, for the definition
  \begin{align*}
  k = \frac{\omega}{c} - i \alpha
  \end{align*}
  to first order, the corresponding Helmholtz equation is
  \begin{align}
  \label{eq:HE:repo}
  \nabla^2 p + \left( \frac{\omega}{c} - i \alpha \right)^2 p = 0
  \end{align}
  which is the equation used for the FEM implementation in this
  repository.
  
  It could be good to have an implementation corresponding to
  Eq. \eqref{eq:k:noapprox}, even to compare results only.
  
  It is interesting to note that the code can be used for the cases
  where Eq. \eqref{eq:approx} is not valid. In these cases you can
  expect, the real part of $k$ (the wavelength) to be be affected by the
  value you set on $\alpha$. If you set a large value of $\alpha$ to
  have an anechoic layer, this is not a problem, since the propagation
  will vanish anyway. However, for other applications, including large
  regions with high attenuation, where the solution for $p$ is relevant,
  it could be necessary to take this effect into consideration.

#+BIBLIOGRAPHY: ../ref plain option:-d

# Local Variables:
# ispell-dictionary: "en"
# End:

#  LocalWords:  compatibilities PML backend eq
