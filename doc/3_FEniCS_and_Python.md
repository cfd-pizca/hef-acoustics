# Simple demos

To get started with the syntax, you could look at [this
demo](demo/normal-modes-rectangular-room), it has only 40 lines of
[code](demo/normal-modes-rectangular-room/demo.py).

The main parts of the script are the following:
- import some FEniCS and standard modules
```python
from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys
```
- import the `hef` module
```python
from hef import solver as H
```
- define the geometry of the system
```python
domain = Rectangle( Point(0.0, 0.0),
                    Point(1.1, 1.0) )  # [m]
source_location = Point(0.7, 0.2)                    
```
- create the mesh
```python
H.mesh = generate_mesh(domain, 100)
```
- initialize the `hef` module
```python
H.init()
```
- define a list of values to sweep over some variable (frequency in this case)
```python
f_list = np.arange(400, 500, 5)
```
- set the output paths
```python
output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p  = File(f'{output_dir}/p.pvd')
```
- loop over the list of values
```python
for H.f in f_list :
    print(f"Working on f={H.f:0.2f} Hz")
    H.update_frequency()
```
  - set boundary conditions and assemble the FEM system
```python
    bc = PointSource(H.V, source_location, 1)
    A, b = H.Ab_assemble(bc)
```
  - solve for pressure
```python
    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)
```
  - save results
```python
    fid_p << (p_c, float(H.f))
```

After that, you can look at [this demo](demo/point-source-2D). It's
almost the same, but it includes a custom definition for
`k_Expression`, wich is used in almost all the demos offered in this
repository. In this case, the purpose of this definition is to set an
absorbing layer, using the imaginary part $`k_i`$ of the wave vector,
which inside the class definition is referred as `value[1]`.

You can also change the real part $`k_r`$ of the wave vector, in two
different ways. First, set the values of `H.c` and `H.f`, after that
call `H.init()`, or call `H.update_frequency()` if `H.init()` has
already been called. For instance, in [this
demo](demo/normal-modes-axisymmetric-simple) you can find an
implementation of this procedure.

On the other hand, a second way to change $`k_r`$, is through a custom
definition of `k_Expression`. You can find an example of this in [this
demo](demo/oblique-incidence), which is a more advanced demo. In this
case, the value a of $`k_r`$, referred within the class defintion as
`value[0]`, depends on the `materials` definition, wich depends on the
spatial coordinates. The corresponding code is the following.

```python
        if self.materials[cell.index] == 2 :
            value[0] = 2*pi*H.f/c2
        else :
            value[0] = 2*pi*H.f/H.c
```

To define "materials", or regions for whose boundaties the physical
properties change abruptly, there are a few options. Check those
options in these demos:
- [define-regions-AutoSubDomain](demo/define-regions-AutoSubDomain)
- [define-regions-set_subdomain](demo/define-regions-set_subdomain)
- [define-regions-simple](demo/define-regions-simple)
- [define-regions-SubDomain](demo/define-regions-SubDomain)

It is also possible to set the values of $`k_r`$, and $`k_i`$
depending on frequency, see [this
demo](demo/absorption-depending-on-frequency). 

All of these things can be done in a 2D simulation correspoding to a
plane of an axisymmetric 3D domain, where the "x axis" is the symmetry
axis of the 3D domain. Possibly, the most simple case is shown in
[this demo](demo/normal-modes-axisymmetric-simple).

After that, the next step is to set different kinds of boundary
conditions. You can find a list of examples in the section "Types of
boundary conditions", in [this file](doc/1_Acoustics.md).

# The `hef` module

## Reference for `hef.solver`

This is where the weak form of the Helmholtz equation is implemented.

### Variables
- `f`: frequency. Default is `100`.
- `c`: speed of sound (this variable is only used for a default value
  set in `k_expression`, see below). Default is `343`.
- `rho`: default mass density, currently used for the admitance and
  vibration boundary conditions, only. Default is `1.2`.
- `axisymmetric`: (`True` or `False`) axial symmetry. Default is `False`.
- `mesh`: (A FEniCS valid mesh) expected to be given, default is
  `None`.
- `k_Expression`: (A subclass of `dolfin.UserExpression`,
  corresponding to a vector of two components). The idea of this class
  is to override the method `eval` in order to set the real `k_r` and
  imaginary `k_i` parts of the wave number. It is also necessary to
  override the methods `__init__` and `value_shape`, as they are in
  the examples.
- `k_expression`: wave number (an instance of the class
  `k_Expression`). To be used in the whole domain. The default is to use
  the value `c` to calculate the corresponding `k_r`, and to use
  `k_i=0.0`.
- `Y_expression`: admitance (a valid `dolfin.Expression`,
  corresponding to a vector of two components). To be used for
  boundaries. The default is `(0.0, 0.0)`. See below[^1].
- `U0_expression`: (a valid `dolfin.Expression`, corresponding to a
  vector of two components). To be used for boundaries. The default is
  `(0.0, 0.0)` See below[^1].

### Methods
- `init`: This method is supposed to be called once. However, in some
  cases (if you change the mesh, for example), you should call this
  method again.
  - arguments:
    - `boundary_Y`: (An instance of some subclass of `SubDomain`,
      corresponding to the boundaries where the admitance `Y` will be
      set).
    - `boundary_U0`: (An instance of some subclass of `SubDomain`
      corresponding to the boundaries where the amplitude `U0` of the
      velocity of a vibrating wall will be set).
    - `V_degree`: (integer) The degree to be used for `V`, see below.
  - return values: As you can see there is no `return` statement in
    this function. However, the function set many module
    variables. Some of the most relevant of these variables are the
    following.
    - `V`: Function space for the trial and test functions.
    - `V0`: Function space for the wave number. See that the degree in
    this case is zero, to allow abrupt changes of the wave number
    between cells.
    - `a_r`: real part of the bilinear form
    - `a_i`: imaginary part of the bilinear form
    - `L_r`: real part of the linear form
    - `L_i`: imaginary part of the linear form
    - `omega`: angular frequency
    - `k_c`: a Function with the actual values (complex) of the wave
      number for the whole mesh, which is used in the linear and
      bilinear forms.
    - `Y_c`: a Function with the actual values (complex) of the
      admitance for the whole mesh, which is used in the linear and
      bilinear forms (on specific boundaries).
    - `U0_c`: a Function with the actual values (complex) of the
      velocity amplitude for the whole mesh, which is used in the
      linear and bilinear forms (on specific boundaries).
- `update_frequency`: The idea of this method is to be smaller version
  of `init`. It will only update variables depending on frequency `f`,
  those are `omega`, `k_c`, `Y_c`, `U0_c`.
- `Ab_assemble`:
  - arguments:
    - `bcs`: (A list of valid FEniCS boundary conditions)
  - return values:
    - `A`: Stiffness matrix
    - `b`: vector to solve, for `u`, the system `Au = b`.
  - see issue #5

##  Issues

https://gitlab.com/cfd-pizca/hef-acoustics/-/issues?label_name%5B%5D=hef+module

## Other files in the `hef` directory

- `custom_mesh_convert.py`: A script to convert a `msh` file (from
  `gmsh`) to `xdmf` format. It is mainly taken from the
  recommendations in the FEniCS forum, slightly modified to be able to
  save specific surfaces, not all cell surfaces. See
  [pulsating-sphere-gmsh](demo/pulsating-sphere-gmsh).

## Engine

Depending on the specifics of the system intended to be simulated, you
could need to use something a variation of FEniCS. You can control this
with the parameter

```python
hef.engine
```

most of the examples provided here set this to `fenics`, or doesn't set
this option, which is the same. The intended engines to be available are

-   `fenics`, which es the same as `dolfin`
-   `dolfin_adjoint`
-   `dolfinx`
-   `firedrake`

However, only `fenics` and `dolfin_adjoint` are currently implemented.

# Handling of complex numbers

If you need real and imaginary parts of pressure in the python
simulation scripts, it can be obtained with
```python
p_r, p_i = p_c.split()
```

See also "Real and imaginary parts of the complex solution"
[here](doc/4_Paraview.md).

- issue #4
- issue #5

# Boundary conditions

- issue #3
- issue #6
- issue #27
- issue #7

## Point source

- issue #12

# Import demos from other python files

This is not currently possible. Most of the demos were chosen to be
written, for simplicity, without a structure of functions.

To be able to import a demo from another python file you can easily
modify it to be something like

```python
# This file is `demo_NN.py`

# import stuff

# Some important variables
parameter = 0
output_dir = "./results"

# A main function
def main () :
   # all the code to run the simulation

# A typical piece of code used to be able to call 
# this script from terminal
if __name__ == "__main__" :
   main()
```

Then, from the other file

```python
# This file is `study_for_NN.py`

# import stuff
import demo_NN

# sweep over some parameter
for parameter in parameter_list :
    # set value
    demo_NN.parameter = parameter
    # change output dir
    demo_NN.output_dir = f"./results/{parameter}"
    # execute
    demo_NN.main()
```

An example of this can be found in [this demo](demo/oblique-incidence).

TODO: include a little discussion on the case you need to sweep over
more than one parameter. Mention paraview time.

# Mesh

## Unable to unrefine

FEniCS have methods to build meshes, and to refine those meshes in
specific regions. However, under some circunstances this is not
enough. See for instance the example [pulsating-sphere-single-octant](demo/pulsating-sphere-single-octant).

### `gmsh` and `meshio`

http://gmsh.info/

This is a tool which allows the creation of meshes, having more
control (than FEniCS tools) over the refinement of specific
regions. It has a python API, wich makes its use specially convenient
along with FEniCS. See example [pulsating-sphere-gmsh](demo/pulsating-sphere-gmsh).

The native format of `gmsh` is the `msh` file, which currently cannot
be directly imported in FEniCS. Some tools exist to convert this
format to something importable in FEniCS. Remarcably `meshio` does
this convertion and so much more.

In this repository, a custom script based in `meshio` is provided to
make the conversion a little different from the default conversion of
`meshio` CLI. That is, to export tagged surfaces alone, wthout the all
the untagged surfaces.

# Uneven frequency lists

In some examples you will find something like
```python
frequency_list = sorted(set(
    list(np.arange(280, 380, 10))
    + list(np.arange(289, 297, 1))
    + list(np.arange(350, 358, 1))
    + [1200, 1300] ))
```

This is a short way to generate a list of frequencies which has
different steps in different regions. The operation works as follows:

Specify one or more even collection of frequencies from `f_min`, to `f_max`
(not including `f_max`), taking steps of size `step`, with
```python
np.arange(f_min, f_max, step)
```
turn them into an actual python `list` with
```python
list(np.arange(f_min, f_max, step))
```
add them together, possibly with other handmade lists, all into a single list, with
```python
list(np.arange(280, 380, 10))
+ list(np.arange(289, 297, 1))
+ list(np.arange(350, 358, 1))
+ [1200, 1300]
```
remove duplicate frequencies generating a python `set` with this list
```python
set(
list(np.arange(280, 380, 10))
+ list(np.arange(289, 297, 1))
+ list(np.arange(350, 358, 1))
+ [1200, 1300] )
```
Finally, generate again a python lists, with the frequencies sorted
```python
frequency_list = sorted(set(
    list(np.arange(280, 380, 10))
    + list(np.arange(289, 297, 1))
    + list(np.arange(350, 358, 1))
    + [1200, 1300] ))
```

# Footnotes

[^1]: When you specify admitance or velocity aplitude for a vibrating
    wall, you could need it to depend on the spatial coordinates. In
    such case, you could do something analogous to the case of the wave
    number `k`. That is to say, for instance, to define a class
    `Y_Expression` depending on `x`, and making a instance
    `Y_expression` of the class `Y_Expression`.



<!--  LocalWords:  TODO bilinear


## `k_Expression` a class for wave number `k`

TODO: Write this

Use `cell value` or `value`


 -->
