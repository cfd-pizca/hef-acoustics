#!/usr/bin/env python3
#

import glob
import re
import pathlib
import os

# TODO: use jinja

# Make sure the file is empty
with open("demo_summary.md",'w') as fd :
    fd.write("\n")

header = """
# Demonstration files

In this file you will find a complete list (automatically generated)
of the available demos, some of them in multiple versions, sorted
alphabetically (by directory name).

In addition, the following files have some discussion on these same
demonstrations from specific perspectives.

"""

with open("demo_summary.md",'a') as fd :
    fd.write(header)
    fd.write("\n\n")

topics_file_list = (
      list(glob.glob(f"doc/*.org"))
    + list(glob.glob(f"doc/*.md" ))
    + list(glob.glob(f"doc/*.pdf")) )

with open("demo_summary.md",'a') as fd :
    for ft in sorted(topics_file_list) :
        stem = pathlib.Path(ft).stem
        stem = stem.replace("_"," ")
        fd.write(f"- [{stem}]({ft})\n")
    fd.write("\n\n")

with open("demo_summary.md",'a') as fd :
    fd.write("---\n\n")
    fd.write("# Demo list\n\n")
    fd.write("---\n\n")

# A demo is a subdirectory of "/demo" with a python file whose name is
# "demo.py" or "demo<something>.py"

# The description of the demo is taken from `description.md`
# (case-sensitive)

# Demos can be in a subdirectory structure, but cannot be nested. In
# the case of nested demos the top most subdirectory is considered,
# and the others are ignored.

# Create demo list
demo_list = []
for f in sorted(pathlib.Path("demo").rglob("demo*.py")) :

    m = re.search('^demo/(.+)/demo.*\.py$', str(f))
    if m :
        demoid = m.group(1)
    else :
        print(f"WARNING: (skipping) `demoid` not found for {f}")
        continue

    demo_list.append(demoid)

# Remove nested demos
for demo1 in demo_list :
    for demo2 in demo_list :
        if demo1 == demo2 : continue
        if re.match(demo1, demo2) :
            demo3 = demo2.replace(demo1,"")
            if re.match("/", demo3) :
                demo_list.remove(demo2)
                print(f"WARNING: {demo2} is nested on {demo1}. {demo2} is ignored.")

demo_data = dict()
for demoid in demo_list :

    if pathlib.Path(f"demo/{demoid}/description.md").is_file() :
        with open(f"demo/{demoid}/description.md", 'r') as fd :
            description_md = fd.read()
    else :
        print(f"WARNING: No description found for {demoid}.")
        description_md = ""

    if re.match("^\s*\#", description_md) :
        description_md = re.sub(
            r"#(.+)\n",
            r"# \1 \n\n" + f"directory name: **{demoid}** \n\n",
            description_md, 1)
    else :
        description_md = f"# `{demoid}`\n\n" + description_md

    screenshot_list = sorted(
        pathlib.Path(f"demo/{demoid}/results").rglob("screenshot*"))
    
    script_list = [
        item
        for item
        in sorted(pathlib.Path(f"demo/{demoid}/").rglob("*.py"))
        if ( not re.match(".+\.\#.+", str(item)) ) ]
    
    features = ""
    has_details = False
    for i, line in enumerate(description_md.splitlines()) :
        if i == 0 :
            title = line
            continue
        if re.match("^\s*\#", line) :
            has_details = True
            break
        else :
            features += line + "\n"

    demo_data[demoid] = {
        "script_list"     : script_list,
        "title"           : title,
        "has_details"     : has_details,
        "features"        : features,
        "description_md"  : description_md,
        "exit_code"       : None,
        "execution_time"  : None,
        "screenshot_list" : screenshot_list}

    with open(f"demo/{demoid}/readme.md",'w') as fd:
        fd.write(description_md + '\n\n')
        if len(screenshot_list) >= 1:
            fd.write("## Screenshots" + '\n')
            for screenshot in screenshot_list :
                fd.write(f'<img src="{screenshot}" height="120">\n')
            fd.write('\n\n')
        fd.write('Scripts:\n')
        for script in script_list :
            basename = os.path.basename(script)
            fd.write(f"- [`{basename}`]({script})\n")
        fd.write('\n\n')
        if pathlib.Path(f"demo/{demoid}/results/vis.pvsm").is_file() :
            fd.write(f"[pvsm paraview file](demo/{demoid}/results/vis.pvsm)" + '\n\n')
        if demo_data[demoid]["execution_time"] is not None :
            et = demo_data[demoid]["execution_time"]
            fd.write(f'Developers execution time: {et}' + '\n')
        fd.write('\n\n')
    
if pathlib.Path("utils/execution_report.txt").is_file() :
    with open("utils/execution_report.txt", 'r') as fd :
        demoid = None
        for line in fd :
            if demoid is None :
                demoid_re = re.search('^-+\s*demo/(.+)/demo.*\.py',line)
                if demoid_re is not None :
                    demoid = demoid_re.group(1)
                    continue
            else :
                ec_re = re.search('^INFO: exit code:\s+([0-9]+)',line)
                if ec_re is not None :
                    demo_data[demoid]["exit_code"] = int(ec_re.group(1))

                et_re = re.search('^real\s+(.+)',line)
                if et_re is not None :
                    demo_data[demoid]["execution_time"] = et_re.group(1)
                    continue

                if ( demo_data[demoid]["execution_time"] is not None and
                     demo_data[demoid]["exit_code"] is not None ) :
                    demoid = None

with open("demo_summary.md",'a') as fd :
    # TODO: sort using the field "title"
    for demoid in demo_data :
        fd.write(demo_data[demoid]["title"] + '\n\n')
        fd.write(demo_data[demoid]["features"] + '\n\n')
        if demo_data[demoid]["has_details"] :
            fd.write(f"**[See more details](demo/{demoid}).**" + '\n\n')
        for screenshot in demo_data[demoid]["screenshot_list"] :
            fd.write(f'<img src="{screenshot}" height="120">\n')
        fd.write('\n\n')
        fd.write('Scripts:\n')
        for script in demo_data[demoid]["script_list"] :
            basename = os.path.basename(script)
            fd.write(f"- [`{basename}`]({script})\n")
        fd.write('\n\n')
        if pathlib.Path(f"demo/{demoid}/results/vis.pvsm").is_file() :
            fd.write(f"[pvsm paraview file](demo/{demoid}/results/vis.pvsm)" + '\n\n')
        if ( demo_data[demoid]["execution_time"] is not None
             and demo_data[demoid]["exit_code"] == 0 ) :
            et = demo_data[demoid]["execution_time"]
            fd.write(f'Developers execution time: {et}' + '\n')
        fd.write('\n\n')
