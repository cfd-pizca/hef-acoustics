Currently the only images built and distributed are those for
- fenics
- dolfin_adjoint

Images are built and distributed for the `master` branch only.
