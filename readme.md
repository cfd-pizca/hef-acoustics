HEF-Acoustics: Helmholtz Equation on FEniCS
===========================================

In this repository you will find an implementation of the Helmholtz
equation for FEniCS in the context of acoustics, with some examples
(demos).

The Helmholtz equation in this case is considered to be

```math
\nabla^2 p + \left(\frac{\omega}{c} - i\alpha\right)^2 p = 0
```

where $`p`$ stands for complex pressure, $`\omega`$ is the angular
frequency, and $`\alpha`$ is the attenuation coefficient.

From an acoustics perspective, the most representative demos can be
found [here](doc/details/list_of_acoustics_systems.md), and a list of
all the available demos is found in this [summary](demo_summary.md).

Disclaimer
------------

This project is **not** part of FEniCS, or Paraview. It is an
independent development. However, it is writen trying to be as
intuitive as possible for the user familiar with these projects.

This project is in its first development stages, then some changes in
the directory and file structure are not uncommon.

Project information
---------

Main repository:
- <https://gitlab.com/cfd-pizca/hef-acoustics>

Authors and contributors:
- Roberto Velasco-Segura [1]
- Julio Octavio Rangel Flores [1]
- Omar Alejandro Bustamante Palacios [1]
- Cristian Ulises Martínez Lule [2]
- Pablo L. Rendón [1]
- Felipe Orduña Bustamante [1]
- Eduardo Romero Vivas [3]
- Augusto García Valenzuela [1]
- Gabriel Eduardo Sandoval Romero [1]
- Guillermo Quintero Pérez [1]
- Alejandro Ortega-Aguilar [1]
- Erika Enedina Martínez Montejo [1]

Adscriptions:
- [1]: Instituto de Ciencias Aplicadas y Tecnología (ICAT), UNAM.
- [2]: Facultad de Ciencias, UNAM.
- [3]: Centro de Investigaciones Biológicas del Noroeste S.C. (CIBNOR).

Funding:
- PAPIIT-UNAM TA100620

Citing
======

This code was presented in the "Acoustical Society of America 181st
Meeting, Seattle, WA, 29 November-3 December 2021.", with the title "A
Helmholtz Equation Implementation for Finite Element Method, Based on
FEniCS Project"

Please refer to this code as "HEF-Acoustics", and cite as:

> R. Velasco-Segura, C. Martínez-Lule, "A Helmholtz Equation
> Implementation for Finite Element Method, Based on FEniCS Project",
> Acoustical Society of America 181st Meeting, Seattle, WA, 29
> November-3 December 2021. https://gitlab.com/cfd-pizca/hef-acoustics

Documentation
=====================

In the code, the values of speed of sound $`c`$, frequency $`f`$, and
attenuation coefficient $`\alpha`$ are handled with a complex valued
wave number

```math
k = k_r + i k_i = \frac{\omega}{c} - i\alpha
```

In other words

```math
\begin{align*}
k_r &= \frac{\omega}{c} = \frac{2\pi f}{c} \\
k_i &= - \alpha
\end{align*}
```

You can find some details for each [demo](demo) in the coorresponding
`readme.md` file. A summary of this same info, with developers
execution times, is found in [this file](demo_summary.md).

Some further discussion from specific perspectives is found in the
files of [this directory](doc).

Finally, a typicall output for running all the examples is found
[here](utils/execution_report.txt).

Installation
=====================

Simulation code
--------------------

To run the simulations in the present repository you will need almost
the same as you wolud need to use
[FEniCS](https://fenicsproject.org/download/). First, install [Docker
CE](https://www.docker.com/products/docker-desktop) for your platform
(Windows, Mac or Linux). After that, possibly the easiest way proceed is to
run the following command:

```bash
curl -s https://get.fenicsproject.org | bash
```

Then, launch a *container* (which is the name typically used for this
kind of virtual machine) capable of running `HEF-Acoustics` with the
command

```bash
fenicsproject run registry.gitlab.com/cfd-pizca/hef-acoustics/fenics
```

The first time you run this command it will download a Docker image
(around 3GB), which is the same as the current stable FEniCS Docker
image, with the code of the present repository, some additional
libraries, and a little configuration.

Later, in the case a new version is available, you can update the
Docker image using the command

```bash
docker pull registry.gitlab.com/cfd-pizca/hef-acoustics/fenics:latest
```

Other methods to install this code are described
[here](doc/details/installation.md). If you want to modify the `hef`
module (which is **not** necesary to run and modify the demos), please
see a recommended installation in this [same
file](doc/details/installation.md).

Visualization
-------------------

Most of the postprocessing in this repository is based in
[Paraview](https://www.paraview.org/), which is **not** installed in
`HEF-Acoustics` Docker images. It is recommended to install Paraview
**not** from a docker image, but as a regular application for your
platform.

To install Paraview for your platform see
[here](https://www.paraview.org/download/).

Usage
=======

Generate data
-------------------

The following commands are meant to be executed within the *container*
described above.

To generate data, you only need to go to the directory corresponding
to one of the demos, and run the corresponding script. For example

```bash
cd $HOME/hef-acoustics/
cd demo/plane-wave/
python3 demo.py
```

However, to be able to see the data from a Paraview instance running
as a regular application on your system, the results need to be in the
shared directory. Then, you could do the following

```bash
cd $HOME
mkdir -p shared/hef-acoustics
cp -a -n hef-acoustics/demo shared/hef-acoustics
cd shared/hef-acoustics/demo/plane-wave/
python3 demo.py
```

Please see that the `-n` flag for the `cp` command is there to prevent
overwriting your existing files.

The last command will generate the data required for the visualization
corresponding to the Paraview state in the file
`shared/hef-acoustics/demo/plane-wave/results/vis.pvsm`.

See the data
------------

In this case, Paraview is assumed to be running as a regular
application of your platform, **not** in the *container*.

In Paraview, use `File > Load State...`, select the corresponding
`vis.pvsm` file (for instance
`hef-acoustics/demo/plane-wave/results/vis.pvsm`), and use the option
`Search files under specified directory`, with the default values. You
should see something like this.

<img src="utils/screenshot.png" height="240">

You can see further details [here](doc/4_Paraview.md).

Contributing
============

The current state of this code is "minimally functional". It is known
important improvements can be made in many areas, including:

- convengence rates
- execution times
- features to include some other important acoustic systems
- coding best practices
- documentation
- distributed execution

An effort has been made to list specific points on these areas. Please
see the [issues
section](https://gitlab.com/cfd-pizca/hef-acoustics/-/issues).

Then, "pull requests" are welcome in any on this topics, or possibly a
(simple) example you could be working on.

If you find errors of any kind, something unclear, or a possible
improvement, please don't hesitate to raise an issue, or making a
comment in the existing issues, even if you do not elaborate much.

Dependencies and compatibilities
================================

This code has been executed with:

- FEniCS,
  - docker image: 2019.1.0.r3
- dolfin adjoint
  - docker image: 9771686177e7
- Paraview:
  - 5.9.0

The code is written for `Python 3.6` or higher.

The refinement, and domain size, of these demos is small enough to run
in a computer with 10GB of memory, taking around 5 min or less, per
demo. For many real applications, more accurate results are needed,
which need some tuning of the code, and more powerful hardware.

You can see the developers actual execution times in [this
file](demo_summary.md).

License
=======

HEF-Acoustics is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

HEF-Acoustics is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.

