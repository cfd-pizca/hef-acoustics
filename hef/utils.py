from dolfin import *
from mshr import *
import numpy as np

def refine_mesh_on(condition_list, mesh, min_dx = 0.001) :

    """Performs a mesh refinement based on a list of conditions.

Parameters:

   condition_list: a function, or a list of functions to be evaluated
   consecutivelly with a single argument `cell.midpoint()`. The mesh
   will be refined for the cells returning `True` in these
   functions. Please note that in the case of 2D
   (`mesh.topology().dim()==2`) the argument of these functions has
   two elements representing the `x` and `y` coordinates of the
   midpoint, and in the 3D case has three elements.
   
   mesh: the mesh to be refined
   
   min_dx: the minimum vale of cell.inradius() acceptable for a
   cell to be refined. The default is 0.

Returns:

   mesh: the refined mesh.

    """

    # Thanks to:
    # https://fenicsproject.discourse.group/t/mesh-refinement-not-working/3199/3
    # https://fenicsproject.org/qa/9760/what-is-the-best-approach-to-mesh-refinement-on-boundary/

    # TODO: should we use `adapt` rather than `refine` here?

    for condition in np.atleast_1d(condition_list) :
        cell_markers = MeshFunction("bool", mesh, mesh.topology().dim())
        cell_markers.set_all(False)
        for cell in cells(mesh):
            if ( condition(cell.midpoint()) and
                 cell.inradius() > min_dx ):
                cell_markers[cell] = True
        mesh = refine(mesh, cell_markers)
    return mesh


