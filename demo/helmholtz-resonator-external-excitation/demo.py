from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H
from hef.utils import refine_mesh_on

# Speed of sound
H.c = 343 # [m/s]

# Domain [m]
width  = 5
height = 5
resonator_width = 0.08

beam_width = height/4

# If this syntax is confusing see "Uneven frequency lists" in
# doc/3_FEniCS_and_Python.md
frequency_list = sorted(set(
    list(np.arange(350, 700, 30))
    + list(np.arange(420, 490, 3) ) ))

output_dir = './results'

pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_k  = File(f'{output_dir}/k.pvd')

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i

    def eval_cell(self, value, x, cell):
        value[0] = self.k_r
        x0b = 5/4
        x1b = 5*(3/4)
        # value[1] is k_i
        if x[0] > x0b or abs(x[1]) > x1b :
            if x[0] > 0 :
                value[1] = - 5*max(
                    [abs(x[0]) - x0b,
                     abs(x[1]) - x1b])**2
            else :
                value[1] = - 5*(abs(x[1]) - x1b)**2
        else :
            value[1] = 0
    def value_shape(self):
        return (2,)

def theoretical_resonance_frequency (c,S,lp,V) :
    # Blackstock "fundamentals of physical acoustics" p. 154. Chap 4,
    # ec. (C-24)
    f = (c/(2*np.pi)) * (S/(lp*V))**0.5
    return f

with open(f"{output_dir}/analytic.csv",'w') as fd:
    # The use of a second column "ampl" here is just for paraview to
    # be able to draw this data in the figure.
    fd.write("freq,ampl\n")
    # See that since this system is 2D, it is not appropriate to apply
    # the end correction for 3D flanged pipes 0.82*r. However, the
    # effective length have indeed an end correction.
    f_H = theoretical_resonance_frequency(
        c  = H.c,
        S  = 2*resonator_width/10,
        lp = resonator_width/2,
        V  = resonator_width*(resonator_width/2) )
    fd.write(f"{f_H:0.2f},0.0001\n")
    fd.write(f"{f_H:0.2f},10\n")

domain = Rectangle(
    Point(-width/2, -height),
    Point( width/2,  height) ) # [m]

# TODO: remove this
wall = Rectangle(
    Point(-width/2, -height),
    Point( width/2, 0.0) ) # [m]

hole = (
    # Body
    Rectangle(
        Point(-resonator_width/2, -resonator_width  ),
        Point( resonator_width/2, -resonator_width/2) )
    # Neck
    + Rectangle(
        Point(-resonator_width/10, -resonator_width/2) ,
        Point( resonator_width/10, 0.0 ) ) ) # [m]

domain = domain - ( wall - hole )

mesh = generate_mesh(domain, 120)

mesh = refine_mesh_on([
    lambda x : ( x[0]**2 + x[1]**2 )**0.5 < width/10,
    lambda x : ( x[0]**2 + x[1]**2 )**0.5 < width/20 ],
    mesh = mesh )

H.k_expression = k_Expression(
    degree=0 )

H.mesh = mesh

class Emitter(SubDomain):
    def inside(self, x, on_boundary) :
        return ( on_boundary and near(x[0], -width/2) )

emitter = Emitter()

P0_expression = Expression(
    ('exp(-0.5*pow((x[1] - mu)/s,N))', '0.0'),
    mu=0,
    s=beam_width/2,
    N=2,
    degree=2 )

H.init()

for H.f in frequency_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()


    bc = [
        DirichletBC(
            H.V,
            P0_expression,
            emitter)]

    A, b     = H.Ab_assemble(bc)
    p_c      = Function(H.V, name="p")
    solve(A, p_c.vector(), b)
    p_r, p_i = p_c.split()
    k_r, k_i = H.k_c.split()

    fid_p << (p_c,   float(H.f))
    fid_k << (H.k_c, float(H.f))
