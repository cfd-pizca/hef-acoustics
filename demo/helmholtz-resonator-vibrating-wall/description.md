# Helmholtz resonator (vibrating wall)

- 2D rectangular geometry
- anechoic walls
- absorbing region
- overriding `k_expression`
- wall velocity boundary condition
- frequency sweep
- resonance
- radiation