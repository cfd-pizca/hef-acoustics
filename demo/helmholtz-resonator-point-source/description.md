# Helmholtz resonator (point source)

- **not working**
- 2D rectangular geometry
- anechoic walls
- point source
- absorbing region
- overriding `k_expression`
- frequency sweep
- resonance
- radiation

## Known issues

The condition of constant pressure in the resonator is broken. This is
posibly related to the shift in resonance frequency.