#  Normal modes in simple axisymmetric domain 

directory name: **normal-modes-axisymmetric-simple** 


- Petri dish geometry
- **modified speed of sound**
- point source
- reflecting walls
- normal modes
- resonance
- axisymmetic
- frequency sweep

## Screenshots
<img src="demo/normal-modes-axisymmetric-simple/results/screenshot_1.png" height="120">
<img src="demo/normal-modes-axisymmetric-simple/results/screenshot_2.png" height="120">
<img src="demo/normal-modes-axisymmetric-simple/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/normal-modes-axisymmetric-simple/demo.py)
- [`screenshots.py`](demo/normal-modes-axisymmetric-simple/screenshots.py)


[pvsm paraview file](demo/normal-modes-axisymmetric-simple/results/vis.pvsm)



