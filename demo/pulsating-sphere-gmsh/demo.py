from dolfin import *
import numpy as np
import pathlib
import sys
import math
import os

# import HEF-Acoustics main module
import hef

# For 3D problems is sometimes good idea to use `dolfin_adjoint`, if
# available you can change this line
hef.engine = "fenics"
# to
# hef.engine = "dolfin_adjoint"

from hef import solver as H
from hef import custom_mesh_convert as cmc

side = 4
R    = 0.125

# Forcing velocity amplitude
U0 = 6.0

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p  = File(f'{output_dir}/p.pvd')
fid_pa = File(f'{output_dir}/pa.pvd')
fid_k  = File(f'{output_dir}/k.pvd')

meshfile = f"{output_dir}/mesh.xdmf"

# Generate a mesh file only if it is not present
if not pathlib.Path(meshfile).is_file() :

    try:
        import gmsh
        import meshio
        import h5py
    except:
        print("""
    Please install some tools with the following commands

    # pip install --user --upgrade h5py
    # pip install --user --upgrade gmsh
    # pip install --user --upgrade meshio
    # sudo apt install gmsh              # <--It will download around 450 MB
    """)
    
    print("INFO: generating mesh files...")
    
    # refinement regions, in descending order
    # items are [name, radius, cellsize]
    refinement = [
        ["exterior", side/2, 0.10],
        ["r2"      , 1.5,    0.08],
        ["r1"      , 1.0,    0.04],
        ["r0"      , 0.5,    0.03],
        ["sphere"  , R,      0.01] ]

    mesh_version = 2.2
    
    gmsh.initialize()
    gmsh.option.setNumber("Mesh.MshFileVersion", mesh_version)
    gmsh.option.setNumber('General.Verbosity', 3)

    gmsh.model.add("air")

    box  = gmsh.model.occ.addBox(
        0, 0, 0,
        side/2, side/2, side/2)

    sphere_r2 = gmsh.model.occ.addSphere(
        0, 0, 0,
        refinement[1][1])

    sphere_r1 = gmsh.model.occ.addSphere(
        0, 0, 0,
        refinement[2][1])

    sphere_r0 = gmsh.model.occ.addSphere(
        0, 0, 0,
        refinement[3][1])

    sphere = gmsh.model.occ.addSphere(
        0, 0, 0,
        R)

    air_exterior = gmsh.model.occ.cut(
        [(3, box)],
        [(3, sphere_r2)],
        removeObject = False,
        removeTool = False)[0][0][1] # ugly

    air_r2 = gmsh.model.occ.cut(
        [(3, box)],
        [(3, air_exterior),
         (3, sphere_r1)],
        removeObject = False,
        removeTool = False)[0][0][1] # ugly

    air_r1 = gmsh.model.occ.cut(
        [(3, box)],
        [(3, air_exterior),
         (3, air_r2),
         (3, sphere_r0)],
        removeObject = False,
        removeTool = False)[0][0][1] # ugly

    air_r0 = gmsh.model.occ.cut(
        [(3, box)],
        [(3, air_exterior),
         (3, air_r2),
         (3, air_r1),
         (3, sphere)],
        removeObject = False,
        removeTool = False)[0][0][1] # ugly

    air_list = [air_exterior, air_r0, air_r1, air_r2]

    fragment_list = [(3, item) for item in air_list]

    # Remove primitive objects.
    gmsh.model.occ.remove(
        [(3, sphere),
         (3, sphere_r0),
         (3, sphere_r1),
         (3, sphere_r2) ] )

    ov, ovv = gmsh.model.occ.fragment([(3, box)], fragment_list)

    f3 = {item[1] for item in fragment_list if item[0] == 3}
    for item in ov :
        if item[0] != 3 : continue
        if item[1] not in f3 :
            box_ov = item[1]
            break

    gmsh.model.occ.synchronize()

    volumes_wanted = set([box_ov] + air_list)
    volumes_present = {vid[1] for vid in gmsh.model.getEntities(dim=3)}
    assert volumes_wanted == volumes_present, "ERROR: volumes are not right"

    gmsh.model.addPhysicalGroup(3, air_list, 1)
    gmsh.model.setPhysicalName(3, 1, "Air")

    for item in refinement :
        Rr = item[1]
        gmsh.model.mesh.setSize(
            gmsh.model.getEntitiesInBoundingBox(
                -Rr*1.1, -Rr*1.1, -Rr*1.1,
                Rr*1.1,   Rr*1.1,  Rr*1.1,
                dim=0),
            item[2])
        gmsh.model.mesh.setSize(
            gmsh.model.getEntitiesInBoundingBox(
                -Rr*1.1, -Rr*1.1, -Rr*1.1,
                Rr*1.1,   Rr*1.1,  Rr*1.1,
                dim=1),
            item[2])

    gmsh.model.mesh.generate(3)

    meshfile_name_noext = os.path.splitext(
        os.path.basename(meshfile))[0]
    
    gmsh.write(f"{output_dir}/{meshfile_name_noext}.msh")
    # # You could use the following files to inspect the mesh and the
    # geometry more easily
    # gmsh.write(f"{output_dir}/{meshfile_name_noext}.geo_unrolled")
    # gmsh.write(f"{output_dir}/{meshfile_name_noext}.vtk")

    gmsh.finalize()

    # Convert msh file to a xdmf file
    cmc.infile = f"{output_dir}/{meshfile_name_noext}.msh"
    cmc.outdir = output_dir
    cmc.main()

    print("INFO: Done.")
        
H.mesh = Mesh()
with XDMFFile(meshfile) as infile:
    infile.read(H.mesh)

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i

    def eval_cell(self, value, x, cell):
        value[0] = self.k_r
        if  abs(x[0]) > 1 or abs(x[1]) > 1 or abs(x[2]) > 1 :
            # This is k_i
            value[1] = - 20*max(
                [abs(x[0]) - 1.0,
                 abs(x[1]) - 1.0,
                 abs(x[2]) - 1.0])**2
        else :
            # This is k_i
            value[1] = 0
    def value_shape(self):
        return (2,)

# Pulsating sphere
# TODO: It would be better to define this surface using gmsh
class Sphere_source(SubDomain):
    def inside(self, x, on_boundary):
        r = (x[0]**2 + x[1]**2 + x[2]**2)**0.5 
        return ( on_boundary and
                 abs(r-R)<0.001 )

sphere_source = Sphere_source()

H.U0_expression = Constant((U0, 0.0))

# If you have enough memory in your system, it is better to use
# `V_degree=2` here.
H.init(
    boundary_U0=sphere_source,
    V_degree=1)

nn = FacetNormal(H.mesh)

print("Let I_1 be: \int_sphere (dp_r/dn) ds / ( omega rho U0 \int_sphere ds )")
print("Let I_2 be: \int_sphere (dp_i/dn) ds / ( omega rho U0 \int_sphere ds )")

# class Analytic_solution(UserExpression) :

#     def __init__(self, k=0.0, **kwargs) :
#         self.k = k
#         super().__init__(**kwargs)

#     def eval(self, value, x):
#         r = (x[0]**2 + x[1]**2 + x[2]**2)**0.5

#         value[0] = (1/r)*cos(self.k*r)
#         value[1] = (1/r)*sin(self.k*r)
        
#     def value_shape(self):
#         return (2,)

H.k_expression = k_Expression(
    degree=0)

H.update_frequency(1000)

# analytic_solution = Analytic_solution()

A, b = H.Ab_assemble()

p_c = Function(H.V, name="p")
solve(A, p_c.vector(), b)

fid_p << (p_c,   float(H.f))
fid_k << (H.k_c, float(H.f))

