raise RuntimeError("Not working.")

from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
import hef

# For 3D problems is sometimes good idea to use `dolfin_adjoint`, if
# available you can change this line
hef.engine = "fenics"
# to
# hef.engine = "dolfin_adjoint"

from hef import solver as H

output_dir = './results'

# Medium variables
#---- air
H.c = 343    # [m/s]

# Frequency
frequency = 40e3

# Absorbing boundary
ab_min = 0
ab_max = 120

# Domain
width  = 0.15 # [m]
height = width/2 # [m]
depth  = width/2 # [m]

# Exciting transducer
# - Position of the center
et_x, et_y, et_z = 0, height/2, depth/2
# - Width
et_width         = 0.02

# Absorbing boundary
ab_r1 = 1.5*et_width
ab_r2 = height/2

# A parameter to define mesh refinement
C_ref = 240

pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p  = File(f'{output_dir}/p.pvd')
fid_k  = File(f'{output_dir}/k.pvd')

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, materials=None, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i
        self.materials = materials

    def ab_set_ki(self, x) :
        """ Imaginary part of k, over the absorbing boundary.
        """

        # set a default value
        k_i = 0.0

        r_yz = ((x[1] - height/2)**2 +
                (x[2] -  depth/2)**2 )**0.5
        
        if r_yz > ab_r1 :
            k_i = - (ab_min
                     + ( (ab_max - ab_min)
                         *(r_yz   - ab_r1)
                         /(ab_r2 - ab_r1)))

        if x[0] > width/2 :
            k_i += - (ab_min
                      + ( (ab_max - ab_min)
                          *(x[0]   - width/2)
                          /(width - width/2)))
            
        return k_i
        
    def eval_cell(self, value, x, cell):
        value[0] = 2*pi*frequency/H.c
        value[1] = self.ab_set_ki(x)

    def value_shape(self):
        return (2,)

H.mesh = BoxMesh(
    Point(0.0, 0.0, 0.0), 
    Point(width, height/2, depth/2),
    C_ref,
    int(round(C_ref*(height/width)/2)),
    int(round(C_ref*(depth/width)/2)))

class Emitter(SubDomain):
    def inside(self, x, on_boundary) :
        r_yz = ( (x[1] - et_y)**2 +
                 (x[2] - et_z)**2 )**0.5
        return ( on_boundary and
                 x[0] <= et_x + DOLFIN_EPS and
                 x[0] >= et_x - DOLFIN_EPS and
                 r_yz <= et_width )
emitter = Emitter()
P0_expression = Expression(
    ('exp(-0.5*pow( pow( pow(x[1]-mu_y,2) + pow(x[2]-mu_z,2), 0.5) /s, N))', '0.0'),
    mu_y=et_y,
    mu_z=et_z,
    s=et_width/2.0,
    N=2,
    degree=2 )

H.f = frequency

# If you have enough memory in your system, it is better to use
# `V_degree=2` here.
H.init(V_degree=1)

bc = [
    DirichletBC(
        H.V,
        P0_expression,
        emitter)]

H.k_expression = k_Expression(
    degree=0 )

p_c      = Function(H.V, name="p")
A, b     = H.Ab_assemble(bc)
solve(A, p_c.vector(), b)
p_r, p_i = p_c.split()
k_r, k_i = H.k_c.split()

fid_p << (p_c,   float(frequency))
fid_k << (H.k_c, float(frequency))

