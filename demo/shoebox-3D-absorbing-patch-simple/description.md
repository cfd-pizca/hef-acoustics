# Illustrate admittance boundary condition (first approach)

- 3D shoebox geometry
- absorbing patch
- wall admittance boundary condition
- point source
- resonance
- normal modes
- frequency sweep

## Published work using this code

> Julio Octavio Rangel Flores, "Estudio numérico de régimen de baja
> frecuencia en medición de absorción en cámara reverberante, aplicado
> al problema de ruido urbano en viviendas", Bachelor dissertation,
> Licenciatura en Ingeniería Eléctrica y Electrónica, Facultad de
> Ingeniería, UNAM, México, 2019. (Spanish)
