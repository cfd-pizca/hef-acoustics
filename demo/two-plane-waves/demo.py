from dolfin import *
from mshr import *
import numpy as np
import sys
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H

domain = Rectangle( Point(-1, -1),
                    Point( 1,  1) )  # [m]

H.mesh = generate_mesh(domain, 100)

# angle [degrees]
theta_list = np.arange(0,91,3)

single_plane_wave = Expression(
    ("cos(k*cos(theta*pi/180)*x[0]+k*sin(theta*pi/180)*x[1] - omega*t)",
     "sin(k*cos(theta*pi/180)*x[0]+k*sin(theta*pi/180)*x[1] - omega*t)"),
    pi = np.pi,
    theta = 0,
    k = 0,
    omega = 0,
    t = 1,
    degree = 2)

H.f = 1000
H.init()

single_plane_wave.k     = H.k_expression.k_r
single_plane_wave.omega = H.omega

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p_first   = File(f'{output_dir}/p_first.pvd')
fid_p_second  = File(f'{output_dir}/p_second.pvd')

for theta in theta_list :

    print(f"Working on theta={theta:0.2f}°")

    single_plane_wave.theta = theta
    p_c_first = Function(H.V, name="p first")
    assign(p_c_first, interpolate(single_plane_wave, H.V))
    fid_p_first << (p_c_first, float(theta))

    single_plane_wave.theta = -theta
    p_c_second = Function(H.V, name="p second")
    assign(p_c_second, interpolate(single_plane_wave, H.V))
    fid_p_second << (p_c_second, float(theta))
    
