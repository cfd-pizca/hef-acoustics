from dolfin import *
from mshr import *
import numpy as np
import sys
import pathlib
import sys
import itertools
import pandas as pd
from scipy import stats
from scipy import special
import yaml

# import HEF-Acoustics main module
from hef import solver as H

testid = sys.argv[0][5:][:-3]

# Forcing velocity amplitude
U0 = 6.0

cyl_R = 0.125

width  = 4.0
height = 4.0

domain = (
    Rectangle(
        Point(-width/2,-height/2),
        Point( width/2, height/2) )
    - Circle(
        Point(0.0, 0.0),
        cyl_R, 40) ) # [m]

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i

    def eval_cell(self, value, x, cell):
        value[0] = self.k_r
        if  abs(x[0]) > 1 or abs(x[1]) > 1 :
            # This is k_i
            value[1] = - 10*max(
                [abs(x[0]) - 1.0,
                 abs(x[1]) - 1.0])**2
        else :
            # This is k_i
            value[1] = 0
    def value_shape(self):
        return (2,)

H.k_expression = k_Expression(
    degree=0)

class Analytic_solution(UserExpression) :
    t = 0
    def __init__(self, t=0, **kwargs) :
        self.t = t
        super().__init__(**kwargs)

    def eval(self, value, x):
        r     = (x[0]**2 + x[1]**2)**0.5
        k     = H.k_expression.k_r
        omega = 2.0*np.pi*H.f
        # TODO: Check the sign here
        complex_solution = - (
            (1j * H.rho * H.c * U0)
            * (  special.hankel2(0, r     * k)
               / special.hankel2(1, cyl_R * k) )
            * np.exp(1j * omega * self.t) )
        value[0] = complex_solution.real
        value[1] = complex_solution.imag

    def value_shape(self):
        return (2,)

analytic_solution = Analytic_solution(degree = 5)

# Pulsating cylinder
class Cyl_source(SubDomain):
    def inside(self, x, on_boundary):
        return ( on_boundary and
                 between(x[0], (-0.3, 0.3)) and
                 between(x[1], (-0.3, 0.3)) )

cyl_source = Cyl_source()

H.U0_expression = Constant((U0, 0.0))

test_data = {
    "i"                      : [],
    "f"                      : [],
    "error_L2"               : [],
    "error_percent"          : [],
    "N"                      : [],
    "wavelength"             : [],
    "cell_inradius_mean"     : [],
    "cell_inradius_std"      : [],
    "cell_circumradius_mean" : [],
    "cell_circumradius_std"  : [],
    "mesh_edge_mean"         : [],
    "mesh_edge_std"          : []}

i_prev = None

N_list = np.logspace(
    np.log10(2),
    np.log10(10),
    8)

f_list = {
    i: f
    for (i, f)
    in enumerate(np.arange(400, 1301, 300)) }

comparisson_domain = AutoSubDomain(
    lambda x:
    abs(x[0]) < 1 and abs(x[1]) < 1)

for n, (i, N) in enumerate(itertools.product(f_list, N_list)) :

    H.f = f_list[i]

    if not i == i_prev :
        output_dir = f'./results/i_{i}'
        pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
        fid_p   = File(f'{output_dir}/p.pvd')
        fid_pa  = File(f'{output_dir}/pa.pvd')
        print(f"-"*80)

    wavelength = H.c/H.f

    print(f"Working on run {n}:")
    print(f"    f = {H.f:0.2f} Hz, i = {i}")
    print(f"    wavelength = {wavelength:0.2f} m, N = {N:0.2f}")

    # It is easier to see: M/N = max(width,height)/wavelength
    M = N*max(width,height)/wavelength

    H.mesh = generate_mesh(domain, M)

    edge_size_list = [
        edge.length()
        for edge
        in edges(H.mesh) ]
    cell_size_list = [
        (cell.inradius(), cell.circumradius())
        for cell
        in cells(H.mesh) ]

    mesh_size_parameters = {
        "cell_inradius_mean"    : float(np.mean([ x[0] for x in cell_size_list])),
        "cell_inradius_std"     : float(np.std( [ x[0] for x in cell_size_list])),
        "cell_circumradius_mean": float(np.mean([ x[1] for x in cell_size_list])),
        "cell_circumradius_std" : float(np.std( [ x[1] for x in cell_size_list])),
        "mesh_edge_mean"        : float(np.mean(edge_size_list)),
        "mesh_edge_std"         : float(np.std( edge_size_list)) }

    sub_domains = MeshFunction(
        "size_t",
        H.mesh,
        H.mesh.topology().dim(),
        H.mesh.domains())
    sub_domains.set_all(0)
    comparisson_domain.mark(sub_domains, 1)
    dxx = Measure('dx', domain=H.mesh, subdomain_data=sub_domains)

    H.init(boundary_U0=cyl_source)
    H.update_frequency()

    A, b = H.Ab_assemble()
    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)
    fid_p << (p_c, float(N))

    pa_c = Function(H.V, name="pa")
    assign(pa_c,  interpolate(analytic_solution, H.V))
    fid_pa << (pa_c, float(N))

    # The use of `dolfin.errornorm` is not possible here because it is
    # currently not implemented for subdomains. See:
    # - https://fenicsproject.org/olddocs/dolfin/1.6.0/python/programmers-reference/fem/norms/errornorm.html
    # - https://fenicsproject.org/qa/9922/how-to-get-errornorm-on-subdomain/
    error_L2 = (assemble(((p_c - pa_c)**2)*dxx(1)))**0.5
    ref      = (assemble(((pa_c)**2)*dxx(1)))**0.5

    error_percent = 100*error_L2/ref

    test_data["i"].append(i)
    test_data["f"].append(H.f)
    test_data["error_L2"].append(error_L2)
    test_data["error_percent"].append(error_percent)
    test_data["N"].append(N)
    test_data["wavelength"].append(wavelength)
    test_data["cell_inradius_mean"].append(
        mesh_size_parameters["cell_inradius_mean"])
    test_data["cell_inradius_std"].append(
        mesh_size_parameters["cell_inradius_std"])
    test_data["cell_circumradius_mean"].append(
        mesh_size_parameters["cell_circumradius_mean"])
    test_data["cell_circumradius_std"].append(
        mesh_size_parameters["cell_circumradius_std"])
    test_data["mesh_edge_mean"].append(
        mesh_size_parameters["mesh_edge_mean"])
    test_data["mesh_edge_std"].append(
        mesh_size_parameters["mesh_edge_std"])

    i_prev = i

df_test_data = pd.DataFrame(test_data)

df_test_data.to_csv(f'./results/test_data.csv')

df = df_test_data.pivot_table(
    index="N",
    columns="i",
    values="error_percent")

df = df.rename(
    columns={
        i: f"(i={i}) f={f_list[i]:0.2f}"
        for i
        in f_list } )

df.to_csv(
    f'./results/error_percent+N+i.csv')

error_percet_avg = [
    (key, np.mean(row))
    for (key, row)
    in df.iterrows() ]

slope, intercept,_,_,_ = stats.linregress(
    [ np.log10(item[0])
      for item
      in error_percet_avg ],
    [ np.log10(item[1])
      for item
      in error_percet_avg ] )
eta = -slope

tendency = {
    "N" : [ item[0] for item in error_percet_avg ],
    "Ep" : [
        (10**intercept)*(item[0]**slope)
        for item
        in error_percet_avg ] }

N1 = (10**(-intercept))**(1/slope)

df_tendency = pd.DataFrame(tendency)
df_tendency.to_csv(f'./results/tendency.csv')

with open(f'./results/parameters.yaml', 'w') as fd :
    yaml.dump({
        "eta": float(eta),
        "N1": float(N1),
        "linregress_intercept" : float(intercept) },
              fd,
              default_flow_style=False)

with open(f'./results/parameters.csv', 'w') as fd :
    fd.write("parameter,value\n")
    fd.write(f"eta, {eta:0.2f}\n")
    fd.write(f"N1, {N1:0.2f}\n")
    fd.write(f"linregress_intercept, {intercept:0.2f}\n")

with open(f'./results/N1.csv', 'w') as fd :
    Ep_min = min(tendency["Ep"])
    Ep_max = max(tendency["Ep"])
    fd.write("N,Ep\n")
    fd.write(f"{N1:0.2f},{Ep_min:0.2f}\n")
    fd.write(f"{N1:0.2f},{Ep_max:0.2f}\n")
