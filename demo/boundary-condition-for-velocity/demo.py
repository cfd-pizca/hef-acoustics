from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H

# Forcing velocity amplitude
U0 = 6.0

cyl_R = 0.25

domain = (
    Rectangle(
        Point(-0.5, -0.5),
        Point(0.7, 0.5) )
    - Circle(
        Point(0.0, 0.0),
        cyl_R, 40) ) # [m]

H.mesh = generate_mesh(domain, 200)

f_list = np.arange(400, 500, 5)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_k  = File(f'{output_dir}/k.pvd')
fid_U0 = File(f'{output_dir}/U0.pvd')

# Pulsating cylinder
class Cyl_source(SubDomain):
    def inside(self, x, on_boundary):
        return ( on_boundary and
                 between(x[0], (-0.3, 0.3)) and
                 between(x[1], (-0.3, 0.3)) )

cyl_source = Cyl_source()

H.U0_expression = Constant((U0, 0.0))

H.init(boundary_U0=cyl_source)

nn = FacetNormal(H.mesh)

with open(f"{output_dir}/boundary_condition_check_1.csv", 'w') as fd :
    fd.write("f,I_r,I_i\n")

# # TODO: fix this, see below
# # pn_r : derivative of p_r in the direction normal to the boundary
# # pn_i : derivative of p_i in the direction normal to the boundary
# with open(f"{output_dir}/boundary_condition_check_2.csv", 'w') as fd :
#     fd.write("f,x,y,angle,pn_r,pn_i\n")

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    A, b = H.Ab_assemble()

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)
    p_r, p_i = p_c.split()

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c, float(H.f))

    if H.f == f_list[0] :
        fid_U0 << H.U0_c

    F_r = dot(grad(p_r), nn)
    F_i = dot(grad(p_i), nn)

    # # TODO: figure out some way to do this
    # # See:
    # # - https://fenicsproject.org/qa/10662/dolfin-iterates-through-elements-during-assembly-process/
    # # - https://fenicsproject.discourse.group/t/iterate-over-all-facets-on-mesh-surface-to-obtain-average-values-for-each-facet/6628
    # # - https://fenicsproject.discourse.group/t/extract-coordinates-of-the-nodes-of-a-subboundary/3429
    # for e_r, e_i in "elements of F_r and F_i" :
    #     with open(f"{output_dir}/boundary_condition_check_2.csv", 'a') as fd :
    #         angle = "something using e_r.x and e_r.y"
    #         fd.write(f"{H.f},{e_r.x},{e_r.y},angle,{e_r.value},{e_i.value}\n")

    I_r = assemble((F_r / ((2*np.pi*H.f)*H.rho*U0 * 2*np.pi*cyl_R)) * H.ds(2))
    I_i = assemble((F_i / ((2*np.pi*H.f)*H.rho*U0 * 2*np.pi*cyl_R)) * H.ds(2))

    with open(f"{output_dir}/boundary_condition_check_1.csv", 'a') as fd :
        fd.write(f"{H.f},{I_r},{I_i}\n")
