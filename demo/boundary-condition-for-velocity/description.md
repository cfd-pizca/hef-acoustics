# Illustrate boundary condition for velocity

- 2D shoebox geometry
- pulsating circle (cylinder)
- wall velocity boundary condition
- frequency sweep
- reflecting walls
- resonance
- normal modes

## Circle or cylinder

This is a 2D simulation over a rectangle, with a circular hole. This
system is equivalent to a 3D duct, with rectangular cross section,
with a cylincer in the middle, where the fields doesn't depend on the
coordinate parallel to the axis of the duct. Then, sometimes in the
code the boundary condition is said to be over "the cylinder".

## Boundary condition check

In order to check the boundary condition "for velocity" imposed over
the circle. For each frequency, the following integrals are computed

```math
I_r = 
\left(
\int_{circle} \frac{\partial p_r}{\partial \hat{n}} ds
\right) \bigg/ \left(
\omega \rho U_0 \int_{circle} ds
\right) \\
I_i = 
\left(
\int_{circle} \frac{\partial p_i}{\partial \hat{n}} ds
\right) \bigg/ \left(
\omega \rho U_0 \int_{circle} ds
\right)
```

Since the boundary condition for velocity is set as a real value $`U_0`$
```math
U = U_0 = U_{0,r} + i U_{0,i} \\
U_{0,r} = U_0 \\
U_{0,i} = 0
```
and
```math
\frac{\partial p_r}{\partial \hat{n}} = - i \omega \rho U_0
```
TODO: include reference to own documentation.

Then, for each frequency, the values of these integrals should be
```math
I_r = 0 \\
I_i = -1
```

You can see a figure of $`I_r`$, and $`I_i`$ as a function of
frequency, in the paraview visualization.

### Improvement of this test

It would be better to save the value of $`\frac{\partial p_r}{\partial
\hat{n}}`$ for multiple points in the circle boundary and check its
value. See comments in the code.
