#  Pulsating cylinder in free space 

directory name: **pulsating-cylinder-cartesian** 


- 2D freespace geometry
- pulsating circle (cylinder)
- wall velocity boundary condition
- anechoic walls
- radiation
- frequency sweep
- analytic solution: (TODO) Check a sign (see code)

## Remarks

### Axial symmetry

This simulation correspond to a system with axial symmetry: a
pulsating cylindrical tube. However, it is *not* coded to use the
axisymmetric feature of the `hef` module. The symmetry axis is not in
the domain, but it is transversal to the domain, the tube is
represented with a a circle in a plane cartesian domain.

## Screenshots
<img src="demo/pulsating-cylinder-cartesian/results/screenshot_1.png" height="120">
<img src="demo/pulsating-cylinder-cartesian/results/screenshot_2.png" height="120">
<img src="demo/pulsating-cylinder-cartesian/results/screenshot_3.png" height="120">
<img src="demo/pulsating-cylinder-cartesian/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/pulsating-cylinder-cartesian/demo.py)
- [`screenshots.py`](demo/pulsating-cylinder-cartesian/screenshots.py)


[pvsm paraview file](demo/pulsating-cylinder-cartesian/results/vis.pvsm)



