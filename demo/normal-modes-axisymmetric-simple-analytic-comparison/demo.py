from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys
from scipy import special

# import HEF-Acoustics main module
from hef import solver as H

H.axisymmetric = True

width  = 0.1 # [m]
radius = 1.0 # [m]

domain = Rectangle( Point(0.0,   0.0   ),
                    Point(width, radius) ) # [m]

H.mesh = generate_mesh(domain, 120)

H.c = 1

H.init()

# TODO: paraview has a problem with normalization for frequencies over
# 2, fix this.
f_list = list(np.arange(0.1, 2.0, 0.03))

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p  = File(f'{output_dir}/p.pvd')
fid_pa  = File(f'{output_dir}/pa.pvd')

alpha_list = dict()

def f_analytic_resonance(n) :
    """ Get frequency for mode (0,n) """
    if n not in alpha_list :
        # TODO: There is possibly a way not to calculate this zeros
        # this many times.
        for nn,a in enumerate(special.jnp_zeros(0, n)) :
            alpha_list[nn+1] = a
    alpha = alpha_list[n]
    # Blackstock, D. T. "Fundamentals of physical acoustics."
    # (2001). Chap. 11 eq. (C-7)
    f = (H.c*alpha)/(2*np.pi*radius)
    return f

class Analytic_solution(UserExpression) :
    n = None
    def __init__(self, **kwargs) :
        super().__init__(**kwargs)
    def eval(self, value, x):
        alpha = alpha_list[self.n]
        value[0] = special.jv(0,alpha*x[1]/radius)
        value[1] = value[0]
    def value_shape(self):
        return (2,)

analytic_solution = Analytic_solution()

# Get all the frequencies for modes in the range defined with f_list
f_ar = dict()
n    = 1
while True :
    f = f_analytic_resonance(n)
    if f >= min(f_list) :
        if f <= max(f_list) :
            f_ar[f] = {
                "n" : n }
            n += 1
        else :
            break
    else :
        n += 1

# Include resonance frequencies in the frequency list to be simulated
f_list = sorted(f_list + [f for f in f_ar])

# Analytic solution
pa_c = Function(H.V, name="pa")

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    # See that because of the axial symmetry this point source is
    # acctually a ring
    bc = PointSource(H.V, Point(0.01, 0.2), 1)

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c, float(H.f))

    if H.f in f_ar :
        print(f"Mode for m:0, n:{f_ar[H.f]['n']}")
        analytic_solution.n = f_ar[H.f]["n"]
        assign(pa_c,  interpolate(analytic_solution, H.V))
        fid_pa << (pa_c, float(H.f))
