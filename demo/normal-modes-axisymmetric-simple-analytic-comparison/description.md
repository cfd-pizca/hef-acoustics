# Normal modes in simple axisymmetric domain (comparison with analytical solution)

- Petri dish geometry
- **modified speed of sound**
- point source
- reflecting walls
- normal modes
- resonance
- axisymmetic
- analytic solution: Bessel functions (for specific frequencies)
- frequency sweep